import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * main fractal class
 */
public class Fractal
{
    public static void main(String[] args)
    {
        final FractalWindow window = new FractalWindow();
        window.validate();
        window.setVisible(true);
    }

}

/**
 * GUI class for fractal demo
 */
class FractalWindow extends JFrame implements ActionListener
{
    private final JPanel contentPane;
    private final JPanel canvas;
    private final BorderLayout borderLayout = new BorderLayout();
    private FractalThread fractal = null;

    /**
     * ctor - builds UI
     */
    public FractalWindow()
    {
        // set up content panel
        contentPane = (JPanel) this.getContentPane();
        contentPane.setLayout(borderLayout);
        setSize(new Dimension(400, 500));
        setResizable(false);
        setTitle("Fractal Demo");

        // set up menu
        final JPanel menuPanel = new JPanel();
        final JButton start = new JButton("start");
        final JButton stop = new JButton("stop");
        start.addActionListener(this);
        stop.addActionListener(this);
        menuPanel.add(start);
        menuPanel.add(stop);
        contentPane.add(menuPanel, BorderLayout.SOUTH);

        // set up canvas
        canvas = new JPanel();
        canvas.setSize(400, 300);
        canvas.setBackground(Color.BLACK);
        contentPane.add(canvas, BorderLayout.CENTER);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * react on events
     *
     * @param e
     */
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();

        if(cmd.equals("start") && fractal == null) {
            fractal = new FractalThread(canvas);
            fractal.start();
        }

        if( cmd.equals("stop") && fractal != null) {
            fractal.pause();
            fractal = null;
        }
    }
}

class FractalThread extends Thread
{
    private boolean running = true;
    private final JPanel canvas;

    public FractalThread(final JPanel canvas)
    {
        this.canvas = canvas;
    }

    public void pause(){
        running = false;
    }

    public void run(){
        runLorenz();
    }

    /**
     * draw lorenz fractal
     */
    private void runLorenz()
    {
        // constants for lorenz fractal
        final double a = 10.0;
        final double b = 8 / 3;
        final double c = 28.0;
        final double d = 0.003;

        // lorenz variables
        double x_new, x_old, y_new, y_old, z_new, z_old;
        int pos_x, pos_y;
        x_old = 0.1;
        y_old = 0.1;
        z_old = 0.5;

        int num = 0;
        final Color[] colors = { Color.RED, Color.GREEN, Color.PINK, Color.YELLOW, Color.BLUE };
        Random rnd = new Random();

        // initialize canvas
        Graphics g = canvas.getGraphics();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 400, 400);
        g.setColor(Color.RED);

        // start drawing
        while (running)
        {
            // calculate new values
            x_new = x_old + d * a * (y_old - x_old);
            y_new = y_old + d * (x_old * (c-z_old) - y_old);
            z_new = z_old + d * (x_old * y_old - b * z_old);

            pos_x = (int) (200 + x_new * 100);
            pos_y = (int) (250 + y_new * 100);

            // draw next point
            if ( pos_x >= 0 && pos_x <= 400 && pos_y >= 0 && pos_y <= 400 )
            {
                g.fillRect(pos_x, pos_y, 1, 1);

                // set up new color
                if ( ++num > 50000 )
                {
                    g.setColor(colors[rnd.nextInt(5)]);
                    num = 0;
                }
            }

            x_old = x_new;
            y_old = y_new;
            z_old = z_new;
        }
    }


}